//
// Created by Titouan ALLAIN on 9/22/21.
//

#ifndef MERGE_H
#define MERGE_H

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "define.h"

void merge(struct TTab u_tab, struct TTab v_tab, struct TTab t_tab);

#endif //MERGE_H
