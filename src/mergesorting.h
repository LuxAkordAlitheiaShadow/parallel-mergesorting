//
// Created by Titouan ALLAIN on 9/22/21.
//

#ifndef MERGESORTING_H
#define MERGESORTING_H

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <stdbool.h>

#include "insertsorting.h"
#include "merge.h"  
#include "define.h"

void merge_sorting(struct TTab t_tab);
void* thread_merge_sorting(void* void_t_tab);

#endif //MERGESORTING_H
