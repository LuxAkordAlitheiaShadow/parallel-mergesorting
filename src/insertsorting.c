//
// Created by Titouan ALLAIN on 9/22/21.
//

#include "insertsorting.h"

//extern int t_size;

void insert_sorting(struct TTab t_tab)
{
    // Declaring variables
    int t_i_index;
    int t_j_index;
    int key;

    // Sorting tab in loop
    for ( t_j_index = 1 ; t_j_index < t_tab.size ; t_j_index++ )
    {
        key = t_tab.tab[t_j_index];
        t_i_index = t_j_index - 1;
        // Shifting value until is greater than key value
        while ( t_i_index >= 0 && t_tab.tab[t_i_index] > key)
        {
            t_tab.tab[t_i_index + 1] = t_tab.tab[t_i_index];
            t_i_index--;
        }
        t_tab.tab[t_i_index + 1] = key;
    }
}