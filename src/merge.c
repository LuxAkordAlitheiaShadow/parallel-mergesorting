//
// Created by Titouan ALLAIN on 9/22/21.
//

#include "merge.h"
#include "define.h"
#include <limits.h>

void merge(struct TTab u_tab, struct TTab v_tab, struct TTab t_tab)
{
    int u_index = 0;
    int v_index = 0;
    int k_index;

    // Adding max value possible for last element of half tabs
    u_tab.tab[u_tab.size] = INT_MAX;
    v_tab.tab[v_tab.size] = INT_MAX;

    // Affecting sorted values in tab to sort in loop
    for ( k_index = 0 ; k_index < t_tab.size ; k_index++ )
    {
        // First half tab element is lower than second half tab element statement
        if ( u_tab.tab[u_index] < v_tab.tab[v_index])
        {
            t_tab.tab[k_index] = u_tab.tab[u_index++];
        }
        // First half tab element is greater than second half tab element statement
        else
        {
            t_tab.tab[k_index] = v_tab.tab[v_index++];
        }
    }
}