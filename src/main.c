//
// Created by Titouan ALLAIN on 9/22/21.
//

#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <stdbool.h>

#include "mergesorting.h"
#include "insertsorting.h"
#include "define.h"


int active_thread;

int main()
{
    printf("Start of the program\n");

    // Mode selection
    bool mode_not_selected = true;
    int mode_used;
    while(mode_not_selected)
    {
        printf("Do you want to parallelize sort ?\n Please enter 0 for No or 1 for Yes : ");
        scanf("%d",&mode_used);
        switch (mode_used)
        {
            case 0:
                printf("Sequential sorting selected\n");
                mode_not_selected = false;
                break;
            case 1:
                printf("Parallel sorting selected\n");
                mode_not_selected = false;
                break;
            default:
                printf("Please enter a valid integer (0 or 1) !");
        }
    }

    // Tab size selection
    printf("Please enter the size of the array : ");
    scanf("%d",&t_tab.size);

    printf("Affecting values to tab...\n");
    // Creating tab to sort
    t_tab.tab = malloc(t_tab.size * sizeof(int));

    // Generating values for tab
    int t_index;
    for( t_index = 0 ; t_index < t_tab.size ; t_index++)
    {
        t_tab.tab[t_index] = rand();
    }

    printf("Sorting tab...\n");
    // Initializing clock
    struct timespec current_time;
    long start_time;
    clock_gettime(CLOCK_REALTIME, &current_time);
    start_time=current_time.tv_sec;

    // Insert sorting statement
    if (t_tab.size < THRESHOLD_INSERT_SORTING)
    {
        insert_sorting(t_tab);
    }
    // Merge sorting statement
    else
    {
        // Parallel sorting statement
        if (mode_used)
        {
            active_thread = 0;
            // Initializing thread id
            pthread_t t_thread;
            // Executing thread
            pthread_create(&t_thread, NULL, thread_merge_sorting, (void *) &t_tab);
            // Waiting thread
            pthread_join(t_thread, NULL);
        }
        // Sequential sorting statement
        else
        {
            merge_sorting(t_tab);
        }
    }
    // Getting end time of program
    long end_time;
    clock_gettime(CLOCK_REALTIME, &current_time);
    end_time=current_time.tv_sec;

    // Calculating execution time
    long execution_time;
    execution_time = end_time - start_time;
    printf("Total execution time : %ld seconds.\n\n", execution_time);

    // Printing 10 first sorted values
    printf("10 first sorted values :");
    for( t_index = 0; t_index < 10 ; t_index++)
    {
        printf("%d ; ",t_tab.tab[t_index]);
    }
    printf("\n");
    // Printing 10 last sorted values
    printf("10 last sorted values :");
    for( t_index = t_tab.size - 10; t_index < t_tab.size ; t_index++)
    {
        printf("%d ; ",t_tab.tab[t_index]);
    }
    printf("\n");

    free(t_tab.tab);
    printf("Exiting of the program.\n");
    // TODO RETURN
}
