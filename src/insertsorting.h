//
// Created by Titouan ALLAIN on 9/22/21.
//

#ifndef INSERTSORTING_H
#define INSERTSORTING_H

#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <time.h>
#include <string.h>

#include "define.h"

void insert_sorting(struct TTab t_tab);

#endif //INSERTSORTING_H
