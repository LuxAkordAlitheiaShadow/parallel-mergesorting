//
// Created by Titouan ALLAIN on 9/22/21.
//

#include "mergesorting.h"

extern int active_thread;

void merge_sorting(struct TTab t_tab)
{
    // Insert sorting statement
    if (t_tab.size < THRESHOLD_INSERT_SORTING)
    {
        insert_sorting(t_tab);
    }
    // Merge sorting statement
    else
    {
        // Declare half tabs
        struct TTab u_tab;
        struct TTab v_tab;

        bool equal_tab;

        // Tab to sort is fairly separable statement
        if ( t_tab.size % 2 == 0 )
        {
            u_tab.size = t_tab.size/2;
            equal_tab = true;
        }
        // Tab to sort isn't fairly separable statement
        else
        {
            u_tab.size = t_tab.size/2 + 1;
            equal_tab = false;
        }

        // Allocating first half tab
        u_tab.tab = malloc((u_tab.size + 1) * sizeof(int));

        // Allocating second half tab
        v_tab.size = t_tab.size/2;
        v_tab.tab = malloc((v_tab.size + 1) * sizeof (int));

        // Initializing half tabs in loop
        int t_index;
        for (t_index=0; t_index<v_tab.size; t_index++)
        {
            u_tab.tab[t_index] = t_tab.tab[t_index];  //Add the first half in U tab
            v_tab.tab[v_tab.size-t_index-1] = t_tab.tab[t_tab.size-t_index-1];  //And the second half in V tab
        }
        // Tab to sort isn't fairly separable statement
        if ( !equal_tab )
        {
            u_tab.tab[t_index] = t_tab.tab[t_index];
        }

        // Sorting the half tabs
        merge_sorting(u_tab);
        merge_sorting(v_tab);

        // Merging sorted half tabs
        merge(u_tab, v_tab, t_tab);

        // Releasing allocated memory
        free(u_tab.tab);
        free(v_tab.tab);
    }
}

void* thread_merge_sorting(void* void_t_tab)
{
    active_thread++;
    printf("THREAD : %d\n",active_thread);

    // Extracting thread args
    struct TTab t_tab = *((struct TTab*) void_t_tab);

    // Insert sorting statement
    if ( t_tab.size < THRESHOLD_INSERT_SORTING )
    {
        insert_sorting(t_tab);
    }
    // Merge sorting statement
    else
    {
        // Initializing half-tabs
        struct TTab u_tab;
        struct TTab v_tab;

        bool equal_tab;

        // Tab to sort is fairly separable statement
        if ( t_tab.size % 2 == 0 )
        {
            u_tab.size = t_tab.size / 2;
            equal_tab = true;
        }
        // Tab to sort isn't fairly separable statement
        else
        {
            u_tab.size = t_tab.size/2 + 1;
            equal_tab = false;
        }
        // Allocating first half tab
        u_tab.tab = malloc((u_tab.size + 1) * sizeof(int));

        // Allocating second half tab
        v_tab.size = t_tab.size/2;
        v_tab.tab = malloc((v_tab.size + 1) * sizeof(int));

        // Initializing half tabs in loop
        int t_index;
        for (t_index=0; t_index<v_tab.size; t_index++)
        {
            u_tab.tab[t_index] = t_tab.tab[t_index];  //Add the first half in U tab
            v_tab.tab[v_tab.size-t_index-1] = t_tab.tab[t_tab.size-t_index-1];  //And the second half in V tab
        }
        // Tab to sort isn't fairly separable statement
        if ( !equal_tab )
        {
            u_tab.tab[t_index] = t_tab.tab[t_index];
        }

        // Sorting the half tabs
        // Enough thread statement
        if ( active_thread < MAX_ACTIVE_THREAD )
        {
            // Initializing thread id
            pthread_t u_thread;
            pthread_t v_thread;
            // Executing threads
            pthread_create(&u_thread, NULL, thread_merge_sorting, &u_tab);
            pthread_create(&v_thread, NULL, thread_merge_sorting, &v_tab);
            // Waiting threads
            pthread_join(u_thread, NULL);
            pthread_join(v_thread, NULL);
        }
        // Too much running thread statement
        else
        {
            merge_sorting(u_tab);
            merge_sorting(v_tab);
        }
        // Merging half-tabs
        merge(u_tab, v_tab, t_tab);
    }
    // Returning sorted tab
    pthread_exit(&t_tab);
}
