//
// Created by Titouan ALLAIN on 9/29/21.
//

#ifndef DEFINE_H
#define DEFINE_H

#define THRESHOLD_INSERT_SORTING 100
#define MAX_ACTIVE_THREAD 4000

struct TTab {
    int* tab;
    int size;
} t_tab;

#endif //DEFINE_H
