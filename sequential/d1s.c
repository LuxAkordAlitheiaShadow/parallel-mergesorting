//
// Created by Titouan ALLAIN on 10/4/21.
//

#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <stdbool.h>
#include <limits.h>

#define THRESHOLD_INSERT_SORTING 1000000

struct TTab {
    int* tab;
    int size;
} t_tab;

void merge(struct TTab u_tab, struct TTab v_tab, struct TTab t_tab)
{
    int u_index = 0;
    int v_index = 0;
    int k_index;

    // Adding max value possible for last element of half tabs
    u_tab.tab[u_tab.size] = INT_MAX;
    v_tab.tab[v_tab.size] = INT_MAX;

    // Affecting sorted values in tab to sort in loop
    for ( k_index = 0 ; k_index < t_tab.size ; k_index++ )
    {
        // First half tab element is lower than second half tab element statement
        if ( u_tab.tab[u_index] < v_tab.tab[v_index])
        {
            t_tab.tab[k_index] = u_tab.tab[u_index++];
        }
            // First half tab element is greater than second half tab element statement
        else
        {
            t_tab.tab[k_index] = v_tab.tab[v_index++];
        }
    }
}

void insert_sorting(struct TTab t_tab)
{
    // Declaring variables
    int t_i_index;
    int t_j_index;
    int key;

    // Sorting tab in loop
    for ( t_j_index = 1 ; t_j_index < t_tab.size ; t_j_index++ )
    {
        key = t_tab.tab[t_j_index];
        t_i_index = t_j_index - 1;
        // Shifting value until is greater than key value
        while ( t_i_index >= 0 && t_tab.tab[t_i_index] > key)
        {
            t_tab.tab[t_i_index + 1] = t_tab.tab[t_i_index];
            t_i_index--;
        }
        t_tab.tab[t_i_index + 1] = key;
    }
}

void merge_sorting(struct TTab t_tab)
{
    // Insert sorting statement
    if (t_tab.size < THRESHOLD_INSERT_SORTING)
    {
        insert_sorting(t_tab);
    }
    // Merge sorting statement
    else
    {
        // Declare half tabs
        struct TTab u_tab;
        struct TTab v_tab;

        bool equal_tab;

        // Tab to sort is fairly separable statement
        if ( t_tab.size % 2 == 0 )
        {
            u_tab.size = t_tab.size/2;
            equal_tab = true;
        }
            // Tab to sort isn't fairly separable statement
        else
        {
            u_tab.size = t_tab.size/2 + 1;
            equal_tab = false;
        }

        // Allocating first half tab
        u_tab.tab = malloc((u_tab.size + 1) * sizeof(int));

        // Allocating second half tab
        v_tab.size = t_tab.size/2;
        v_tab.tab = malloc((v_tab.size + 1) * sizeof (int));

        // Initializing half tabs in loop
        int t_index;
        for (t_index=0; t_index<v_tab.size; t_index++)
        {
            u_tab.tab[t_index] = t_tab.tab[t_index];  //Add the first half in U tab
            v_tab.tab[v_tab.size-t_index-1] = t_tab.tab[t_tab.size-t_index-1];  //And the second half in V tab
        }
        // Tab to sort isn't fairly separable statement
        if ( !equal_tab )
        {
            u_tab.tab[t_index] = t_tab.tab[t_index];
        }

        // Sorting the half tabs
        merge_sorting(u_tab);
        merge_sorting(v_tab);

        // Merging sorted half tabs
        merge(u_tab, v_tab, t_tab);

        // Releasing allocated memory
        free(u_tab.tab);
        free(v_tab.tab);
    }
}

int main()
{
    printf("Start of the program\n");
    printf("Sequential sorting selected\n");

    // Tab size selection
    printf("Please enter the size of the array : ");
    scanf("%d",&t_tab.size);

    printf("Affecting values to tab...\n");
    // Creating tab to sort
    t_tab.tab = malloc(t_tab.size * sizeof(int));

    // Generating values for tab
    int t_index;
    for( t_index = 0 ; t_index < t_tab.size ; t_index++)
    {
        t_tab.tab[t_index] = rand();
    }

    printf("Sorting tab...\n");
    // Initializing clock
    struct timespec current_time;
    long start_time;
    clock_gettime(CLOCK_REALTIME, &current_time);
    start_time=current_time.tv_sec;

    // Insert sorting statement
    if (t_tab.size < THRESHOLD_INSERT_SORTING)
    {
        insert_sorting(t_tab);
    }
        // Merge sorting statement
    else
    {
        // Sequential sorting
        merge_sorting(t_tab);
    }

    // Getting end time of program
    long end_time;
    clock_gettime(CLOCK_REALTIME, &current_time);
    end_time=current_time.tv_sec;

    // Calculating execution time
    long execution_time;
    execution_time = end_time - start_time;
    printf("Total execution time : %ld seconds.\n\n", execution_time);

    // Printing 10 first sorted values
    printf("10 first sorted values :");
    for( t_index = 0; t_index < 10 ; t_index++)
    {
        printf("%d ; ",t_tab.tab[t_index]);
    }
    printf("\n");
    // Printing 10 last sorted values
    printf("10 last sorted values :");
    for( t_index = t_tab.size - 10; t_index < t_tab.size ; t_index++)
    {
        printf("%d ; ",t_tab.tab[t_index]);
    }
    printf("\n");

    free(t_tab.tab);
    printf("Exiting of the program.\n");
    return 0;
}